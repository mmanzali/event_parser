include_directories(
  ${EVENT_PARSER_SOURCE_DIR}/include
  ${Boost_INCLUDE_DIRS}
)

add_executable(
  main
  main.cpp
)

target_link_libraries(
  main
  ${Boost_LIBRARIES}
)