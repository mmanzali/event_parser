#include <fstream>
#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include "f_dataformat.hpp"
#include "s_dataformat.hpp"

namespace po = boost::program_options;
namespace fs = boost::filesystem;

int main(int argc, char* argv[]) {
  po::options_description desc("options");
  fs::path filename;

  desc.add_options()("help,h", "print help messages.")(
      "filename,f", po::value<boost::filesystem::path>(&filename)->required(),
      "event filename.");

  try {
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      exit(EXIT_SUCCESS);
    }

    po::notify(vm);
  } catch (po::error const& e) {
    std::cerr << e.what() << '\n' << desc << std::endl;
    exit(EXIT_FAILURE);
  }

  if (!fs::exists(filename)) {
    std::cerr << filename.string() << " does not exist\n";
    return EXIT_FAILURE;
  } else if (!fs::is_regular_file(filename)) {
    std::cerr << filename.string() << " is not a regular file\n";
    return EXIT_FAILURE;
  }

  size_t const filesize = fs::file_size(filename);
  std::vector<char> event(filesize);

  std::ifstream input(filename.c_str(), std::ios::binary);
  input.read(event.data(), event.size());

  dataformat::FileHeader const h = dataformat::get_file_header(event.data());
  // dataformat::print_file_header(h);

  event.erase(std::begin(event), std::begin(event) + h.parsed_size);

  dataformat::EventSegment const e =
      dataformat::get_event_segment(event.data());
  dataformat::print_event_segment(e);

  event.erase(std::begin(event), std::begin(event) + e.parsed_size);

  dataformat::MCParticleSegment const p =
      dataformat::get_mcparticle_segment(event.data());
  dataformat::print_mcparticle_segment(p);

  event.erase(std::begin(event), std::begin(event) + p.parsed_size);

  dataformat::TrackSegment const t =
      dataformat::get_track_segment(event.data());
  dataformat::print_track_segment(t);
  
  event.erase(std::begin(event), std::begin(event) + t.parsed_size);

  return EXIT_SUCCESS;
}
