#!/usr/bin/env python

import argparse
import errno
import os
import event_model
import numpy as np
import itertools


class Efficiency(object):

    def __init__(self, t2p, p2t, particles, event, label):
        self.label = label
        self.n_particles = 0
        self.n_reco = 0
        self.n_pure = 0
        self.n_clones = 0
        self.n_events = 0
        self.n_heff = 0
        self.n_hits = 0
        self.recoeff = []
        self.purity = []
        self.hiteff = []
        self.recoeffT= 0.0
        self.purityT = 0.0
        self.hiteffT = 0.0
        self.avg_recoeff = 0.0
        self.avg_purity = 0.0
        self.avg_hiteff = 0.0
        self.add_event(t2p, p2t, particles, event)

    def add_event(self, t2p, p2t, particles, event):
        self.n_events += 1
        self.n_particles += len(particles)
        self.n_reco += len(reconstructed(p2t))
        self.recoeff.append(1.*self.n_reco/self.n_particles)
        self.n_clones += sum([len(t)-1 for t in clones(t2p).values()])
        hit_eff = hit_efficinecy(t2p, event.hit_to_mcp, event.mcp_to_hits)
        purities = [pp[0] for _, pp in t2p.iteritems() if pp[1] is not None]
        self.n_pure +=np.sum(purities)
        self.n_heff +=np.sum(hit_eff.values())
        self.n_hits +=len(hit_eff)
        if len(hit_eff) > 0: self.hiteff.append(np.mean(hit_eff.values()))
        if len(purities) > 0: self.purity.append(np.mean(purities))
        if len(self.recoeff) > 0: self.avg_recoeff = 100.*np.mean(self.recoeff)
        if len(self.purity) > 0: self.avg_purity = 100.*np.mean(self.purity)
        if len(self.hiteff) > 0: self.avg_hiteff = 100.*np.mean(self.hiteff)
        if self.n_particles > 0:
            self.recoeffT = 100. * self.n_reco / self.n_particles
        if self.n_reco > 0:
            self.purityT = 100. * self.n_pure / (self.n_reco + self.n_clones)

    def __str__(self):
        s = "%18s : %8d from %8d (%5.1f%%, %5.1f%%) %8d clones (%6.2f%%), purity: (%6.2f%%, %6.2f%%),  hitEff: (%6.2f%%, %6.2f%%)"%(self.label
            , self.n_reco, self.n_particles, self.recoeffT, self.avg_recoeff
            , self.n_clones, 100.*self.n_clones/self.n_reco, self.purityT
            , self.avg_purity,self.avg_hiteff, 100.*self.n_heff/self.n_hits)
        return s

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

def update_efficiencies(eff, event, tracks, weights, label, cond):
    #t2p_filtered = {t:(w,p) for t,(w,p) in t2p.iteritems() if (p is None) or cond(p)}
    particles_filtered = {p for  p in event.particles if cond(p)}
    pidx_filtered = [-1]
    if len(particles_filtered) > 0:
        pidx_filtered, particles_filtered = zip(*[(ip,p) for ip, p in enumerate(event.particles) if cond(p)])
    else:
        return eff
    weights_filtered = weights[:,np.array(list(pidx_filtered))]
    t2p,p2t = hit_purity(tracks, particles_filtered, weights_filtered)
    if eff is None:
        eff = Efficiency(t2p, p2t, particles_filtered, event, label)
    else:
        eff.add_event(t2p, p2t, particles_filtered, event)
    return eff

def comp_weights(tracks, event):
    """
    Compute w(t,p)
    The fraction of hits a particle p contributes to all hits of a track t.

    Keyword arguments:
    tracks -- a list of reconstructed tracks
    event -- an insance of event_model.Event holding all information related to this event.
    """
    w = np.zeros((len(tracks), len(event.particles)))
    for i, j in itertools.product(range(len(tracks)), range(len(event.particles))):
        trackhits = tracks[i].hits
        nhits = len(trackhits)
        particle = event.particles[j]
        nhits_from_p = len([h for h in trackhits if event.hit_to_mcp[h].count(particle) > 0])
        w[i,j] = float(nhits_from_p)/nhits
    return w

def hit_purity(tracks, particles, weights):
    """
    Construct purity and reconstruction tables
    This function generates two dicts. The first stores the purities for all
    tracks. A track that has no particle associated (max(w(t,p)) <= 0.7) still
    stores the max particle weight but has no particle associated.
    The second table stores for each particle track for which this particle header
    the maximum weight. If none of its weights were > 0.7 it said to be not
    reconstructed.

    Keyword arguments:
    tracks -- a list of reconstructed tracks
    particles -- a list of Monte-Carlo particles
    weights -- the w(t,p) table calculated with comp_weights
    """
    # initialize hit purities for tracks t.
    t2p = {t:(0.0, None) for t in tracks}
    # initialize reconstruction table for particles p.
    p2t = {p:(0.0, None) for p in particles}
    for i in range(len(tracks)):
        # for each track get all particle weights and detmine max
        wtp, nwtp = np.max(weights[i,:]), np.argmax(weights[i,:])
        if wtp > 0.7:
            t2p[tracks[i]] = (wtp, particles[nwtp])
        else:
            # store the weight anyway but don't associate a particle since this
            # track has no particle associated (i.e. it is a ghost)
            t2p[tracks[i]] = (wtp, None)
    for i in range(len(particles)):
        wtp, nwtp = np.max(weights[:,i]), np.argmax(weights[:,i])
        if wtp > 0.7:
            p2t[particles[i]] = (wtp, tracks[nwtp])
        else:
            p2t[particles[i]] = (wtp, None)
    return t2p, p2t

def hit_efficinecy(t2p, hit_to_mcp, mcp_to_hits):
    """
    Hit efficiency for associated tracks.
    Calculate the hit efficiency for pairs of tracks and their associated
    particle (if any exists).

    Keyword arguments:
    t2p -- hit purity table as caclulated by hit_purity()
    hit_to_mcp -- hit to MC particles dictionary from event data structure
    mcp_to_hits -- MC particle to hits dictionary from event data structure
    """
    hit_eff = {}
    for track, (_, particle) in t2p.iteritems():
        if particle is None:
            continue
        # for each track that is associated to a particle

        # number of hits from particle on track
        hits_p_on_t = sum([hit_to_mcp[h].count(particle) for h in track.hits])
        # # hits from p on t / total # hits from p
        hit_eff[(track, particle)] = float(hits_p_on_t)/len(mcp_to_hits[particle])
    return hit_eff


def reconstructed(p2t):
    "Returns all reconstructed tracks"
    return [t for _,(_,t) in  p2t.iteritems() if t is not None]


def clones(t2p):
    "Returns dictionary of particles that have clones (multiple track assocs)"
    p2t = {}
    for track, (_, particle) in t2p.iteritems():
        if particle is not None:
            # for each associated track
            if particle not in p2t:
                # if this is the first time we see this particle, initialize
                p2t[particle] = []
            # add this track to the list of tracks associated with this particle
            p2t[particle].append(track)
    # if more than one track is associated with a particle, it is a clone.
    return {p:t for p,t in p2t.iteritems() if len(t) > 1}

def ghosts(t2p):
    "Return ghosts, i.e. list of tracks with no particle associated"
    return [t for t,pp in t2p.iteritems() if pp[1] is None]

def ghost_rate(t2p):
    "Returns the fraction of unassociated tracks (fake tracks) and number of ghosts"
    ntracks = len(t2p.keys())
    nghosts = len(ghosts(t2p))
    return float(nghosts)/ntracks, nghosts

def main():
    """The main function"""
    parser = argparse.ArgumentParser()
    parser.add_argument('trackfiles', metavar='TRACKFILE', nargs='+', type=str, help='txt or bin track file to be validated')
    parser.add_argument('-e', '--events', metavar='EVENTFILE', nargs='+', type=str, help='event file to be used in conjunction with txt track file')
    parser.add_argument('-v', '--verbose', dest="verbose", action="store_true", default=False)
    parser.add_argument('-t', '--textracks', action="store_true", default=False,
            help='treat track file as text (default: binary)')
    parser.add_argument('-f', '--argfiles', action="store_true", default=False,
            help='treat track and event file arguments as text files containing filepaths to actual data (default: off)')
    args = parser.parse_args()

    verbose = args.verbose

    for trackfile in args.trackfiles:
        if not os.path.exists(trackfile):
            print("File doesn't exist: %s." % trackfile)
            exit(errno.ENOENT)

    trackfiles = []
    eventfiles = []
    if args.argfiles:
        for argfile in args.trackfiles:
            with open(argfile) as f:
                for line in f:
                    trackfile = line.strip()
                    if not os.path.exists(trackfile):
                        print("Track file doesn't exist: %s." % trackfile)
                        exit(errno.ENOENT)
                    trackfiles.append(trackfile)
        if args.textracks:
            for argfile in args.events:
                with open(argfile) as f:
                    for line in f:
                        eventfile = line.strip()
                        if not os.path.exists(eventfile):
                            print("Evnet file doesn't exist: %s." % eventfile)
                            exit(errno.ENOENT)
                        eventfiles.append(eventfile)
    else:
        trackfiles = args.trackfiles
        eventfiles = args.events

    tracking_data = []
    if verbose:
        print("Reading data: ")
    if args.textracks:
        if args.events is None or len(trackfiles) != len(eventfiles):
            print("Error: Same number of event and txt track files must be provided.")
            exit(errno.ENOENT)
        for trackfile, eventfile in zip(trackfiles, eventfiles):
            event = event_model.read_datfile(eventfile)
            tracks = event_model.read_txt_trackfile(trackfile, event)
            tracks = list(tracks) # here we prefer tracks to be a list.
            tracking_data.append((event, tracks))
    else:
        for trackfile in trackfiles:
            event, tracks = event_model.read_bin_trackfile(trackfile)
            tracks = list(tracks) # here we prefer tracks to be a list.
            tracking_data.append((event, tracks))

    if verbose:
        print(" done.")
    n_tracks = 0
    avg_ghost_rate = 0.0
    n_allghsots = 0
    eff_velo = None
    eff_long = None
    eff_long5 = None
    eff_long_strange = None
    eff_long_strange5 = None
    eff_long_fromb = None
    eff_long_fromb5 = None
    for event, tracks in tracking_data:
        n_tracks += len(tracks)
        weights = comp_weights(tracks, event)
        t2p, _ = hit_purity(tracks, event.particles, weights)
        grate, nghosts = ghost_rate(t2p)
        n_allghsots += nghosts
        avg_ghost_rate += grate
        eff_velo = update_efficiencies(eff_velo, event, tracks, weights, 'velo'
                    , lambda p: p.isvelo and (abs(p.pid) != 11))
        eff_long = update_efficiencies(eff_long, event, tracks, weights, 'long'
                    , lambda p: p.islong and (abs(p.pid) != 11))
        eff_long5 = update_efficiencies(eff_long5, event, tracks, weights, 'long>5GeV'
                    , lambda p: p.islong and p.over5 and (abs(p.pid) != 11))
        eff_long_strange = update_efficiencies(eff_long_strange, event, tracks, weights, 'long_strange'
                    , lambda p: p.islong and p.strangelong and (abs(p.pid) != 11))
        eff_long_strange5 = update_efficiencies(eff_long_strange5, event, tracks, weights, 'long_strange>5GeV'
                    , lambda p: p.islong and p.over5 and p.strangelong and (abs(p.pid) != 11))
        eff_long_fromb = update_efficiencies(eff_long_fromb, event, tracks, weights, 'long_fromb'
                    , lambda p: p.islong and p.fromb and (abs(p.pid) != 11))
        eff_long_fromb5 = update_efficiencies(eff_long_fromb5, event, tracks, weights, 'long_fromb>5GeV'
                    , lambda p: p.islong and p.over5 and p.fromb and (abs(p.pid) != 11))

    nevents = len(tracking_data)

    #print("Average reconstruction efficiency: %6.2f%% (%d #tracks of %d reconstructible particles)"%(100*avg_recoeff/nevents, n_allreco, n_allparticles))
    print("%d tracks including %8d ghosts (%5.1f%%). Event average %5.1f%%"
        %(n_tracks, n_allghsots, 100.*n_allghsots/n_tracks, 100.*avg_ghost_rate/nevents))
    #print("Number of clones: %d"%n_clones)
    #print("Average hit purity: %6.2f%%"%(100*avg_purity/nevents))
    #print("Average hit efficiency: %6.2f%%"%(100*avg_hiteff/nevents))
    print(eff_velo)
    print(eff_long)
    print(eff_long5)
    print(eff_long_strange)
    print(eff_long_strange5)
    print(eff_long_fromb)
    print(eff_long_fromb5)

if __name__ == "__main__":
    main()
