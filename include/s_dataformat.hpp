#ifndef S_DATAFORMAT_HPP
#define S_DATAFORMAT_HPP

#include <string>
#include <vector>

/*
 * Description of events dataformat:
 * https://gitlab.cern.ch/oawile/PrEventDumper/blob/master/DATAFORMAT.md
 *
 * |------------------------|
 * |       FileHeader       |
 * |------------------------|
 * |      EventSegment      |
 * |------------------------|
 * |    MCParticleSegment   |
 * |------------------------|
 * |      TrackSegment      |
 * |------------------------|
 *
 */

namespace dataformat {

/*** File Header ***/

struct FileHeader {
  // uint32_t funcNameLen;
  std::string funcName;  // funcNameLen chars
  uint32_t dataSize;
  ssize_t parsed_size;  // amount of bytes parsed
};

/*** Event Segment ***/

struct EventSegment {
  uint32_t no_sensors;
  uint32_t no_hits;
  std::vector<int32_t> sensor_Zs;          // no_sensors elements
  std::vector<uint32_t> sensor_hitStarts;  // no_sensors elements
  std::vector<uint32_t> sensor_hitNums;    // no_sensors elements
  std::vector<uint32_t> hit_IDs;           // no_hits elements
  std::vector<float> hit_Xs;               // no_hits elements
  std::vector<float> hit_Ys;               // no_hits elements
  std::vector<float> hit_Zs;               // no_hits elements
  ssize_t parsed_size;                     // amount of bytes parsed
};

// N.B. sensor_Zs can be negative (so it should be int32_t and not uint32_t)

/*** Monte Carlo Particle Segment ***/

struct MCParticleStruct {
  uint32_t mcp_key;
  int32_t mcp_id;
  float mcp_p;
  float mcp_pt;
  float mcp_eta;
  float mcp_phi;
  char mcp_islong;       // treated as bool
  char mcp_isdown;       // treated as bool
  char mcp_isvelo;       // treated as bool
  char mcp_isut;         // treated as bool
  char mcp_strangelong;  // treated as bool
  char mcp_strangedown;  // treated as bool
  char mcp_fromb;        // treated as bool
  char mcp_fromd;        // treated as bool
  uint32_t no_hits;
  std::vector<uint32_t> hitIDs;  // no_hits elements
};

// N.B. mcp_id can be negative (so it should be int32_t and not uint32_t)

struct MCParticleSegment {
  uint32_t no_mcp;
  std::vector<MCParticleStruct> particles;  // no_mcp elements
  ssize_t parsed_size;                      // amount of bytes parsed
};

/*** Track Segment ***/

struct TrackStruct {
  uint32_t no_hits;
  std::vector<uint32_t> hitIDs;  // no_hits elements
};

struct TrackSegment {
  uint32_t no_tracks;
  std::vector<TrackStruct> tracks;  // no_tracks elements
  ssize_t parsed_size;              // amount of bytes parsed
};

/*** File Format ***/

struct FileFormat {
  FileHeader file_header;
  EventSegment event_segment;
  MCParticleSegment particle_segment;
  TrackSegment track_segment;
};

}  // namespace dataformat

#endif
