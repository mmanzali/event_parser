#ifndef F_DATAFORMAT_HPP
#define F_DATAFORMAT_HPP

#include <iostream>

#include "s_dataformat.hpp"

namespace dataformat {

/*** File Header ***/

inline FileHeader get_file_header(char* pointer) {
  char* start = pointer;
  FileHeader h;
  uint32_t const funcNameLen = *reinterpret_cast<uint32_t*>(pointer);
  pointer += sizeof(uint32_t);
  h.funcName = {pointer, pointer + funcNameLen};
  pointer += funcNameLen;  // * sizeof(char) -> 1
  h.dataSize = *reinterpret_cast<uint32_t*>(pointer);
  pointer += sizeof(uint32_t);
  h.parsed_size = std::distance(start, pointer);
  return h;
}

inline void print_file_header(FileHeader const& h) {
  std::cout << "\n*** File Header ***\n" << std::endl;
  std::cout << "function name: " << h.funcName << std::endl;
  std::cout << "file size: " << h.dataSize << std::endl;
}

/*** Event Segment ***/

inline EventSegment get_event_segment(char* pointer) {
  char* start = pointer;
  EventSegment e;
  e.no_sensors = *reinterpret_cast<uint32_t*>(pointer);
  pointer += sizeof(uint32_t);
  e.no_hits = *reinterpret_cast<uint32_t*>(pointer);
  pointer += sizeof(uint32_t);
  e.sensor_Zs = {
      reinterpret_cast<int32_t*>(pointer),
      reinterpret_cast<int32_t*>(pointer + e.no_sensors * sizeof(int32_t))};
  pointer += e.no_sensors * sizeof(int32_t);
  e.sensor_hitStarts = {
      reinterpret_cast<uint32_t*>(pointer),
      reinterpret_cast<uint32_t*>(pointer + e.no_sensors * sizeof(uint32_t))};
  pointer += e.no_sensors * sizeof(uint32_t);
  e.sensor_hitNums = {
      reinterpret_cast<uint32_t*>(pointer),
      reinterpret_cast<uint32_t*>(pointer + e.no_sensors * sizeof(uint32_t))};
  pointer += e.no_sensors * sizeof(uint32_t);
  e.hit_IDs = {
      reinterpret_cast<uint32_t*>(pointer),
      reinterpret_cast<uint32_t*>(pointer + e.no_hits * sizeof(uint32_t))};
  pointer += e.no_hits * sizeof(uint32_t);
  e.hit_Xs = {reinterpret_cast<float*>(pointer),
              reinterpret_cast<float*>(pointer + e.no_hits * sizeof(float))};
  pointer += e.no_hits * sizeof(float);
  e.hit_Ys = {reinterpret_cast<float*>(pointer),
              reinterpret_cast<float*>(pointer + e.no_hits * sizeof(float))};
  pointer += e.no_hits * sizeof(float);
  e.hit_Zs = {reinterpret_cast<float*>(pointer),
              reinterpret_cast<float*>(pointer + e.no_hits * sizeof(float))};
  pointer += e.no_hits * sizeof(float);
  e.parsed_size = std::distance(start, pointer);
  return e;
}

inline void print_event_segment(EventSegment const& e) {
  std::cout << "\n*** Event Segment ***\n" << std::endl;
  std::cout << "no_sensors: " << e.no_sensors << std::endl;
  std::cout << "no_hits: " << e.no_hits << std::endl;
  std::cout << "sensor_Zs:\n";
  for (auto const& i : e.sensor_Zs) {
    std::cout << "  " << i << "\n";
  }
  std::cout << "sensor_hitStarts:\n";
  for (auto const& i : e.sensor_hitStarts) {
    std::cout << "  " << i << "\n";
  }
  std::cout << "sensor_hitNums:\n";
  for (auto const& i : e.sensor_hitNums) {
    std::cout << "  " << i << "\n";
  }
  std::cout << "hit_IDs:\n";
  for (auto const& i : e.hit_IDs) {
    std::cout << "  " << i << "\n";
  }
  std::cout << "hit_Xs:\n";
  for (auto const& i : e.hit_Xs) {
    std::cout << "  " << i << "\n";
  }
  std::cout << "hit_Ys:\n";
  for (auto const& i : e.hit_Ys) {
    std::cout << "  " << i << "\n";
  }
  std::cout << "hit_Zs:\n";
  for (auto const& i : e.hit_Zs) {
    std::cout << "  " << i << "\n";
  }
}

/*** Monte Carlo Particle Segment ***/

inline MCParticleSegment get_mcparticle_segment(char* pointer) {
  char* start = pointer;
  MCParticleSegment p;
  p.no_mcp = *reinterpret_cast<uint32_t*>(pointer);
  pointer += sizeof(uint32_t);
  for (uint32_t i = 0; i < p.no_mcp; ++i) {
    MCParticleStruct s;
    s.mcp_key = *reinterpret_cast<uint32_t*>(pointer);
    pointer += sizeof(uint32_t);
    s.mcp_id = *reinterpret_cast<int32_t*>(pointer);
    pointer += sizeof(int32_t);
    s.mcp_p = *reinterpret_cast<float*>(pointer);
    pointer += sizeof(float);
    s.mcp_pt = *reinterpret_cast<float*>(pointer);
    pointer += sizeof(float);
    s.mcp_eta = *reinterpret_cast<float*>(pointer);
    pointer += sizeof(float);
    s.mcp_phi = *reinterpret_cast<float*>(pointer);
    pointer += sizeof(float);
    s.mcp_islong = *reinterpret_cast<char*>(pointer);
    pointer += sizeof(char);
    s.mcp_isdown = *reinterpret_cast<char*>(pointer);
    pointer += sizeof(char);
    s.mcp_isvelo = *reinterpret_cast<char*>(pointer);
    pointer += sizeof(char);
    s.mcp_isut = *reinterpret_cast<char*>(pointer);
    pointer += sizeof(char);
    s.mcp_strangelong = *reinterpret_cast<char*>(pointer);
    pointer += sizeof(char);
    s.mcp_strangedown = *reinterpret_cast<char*>(pointer);
    pointer += sizeof(char);
    s.mcp_fromb = *reinterpret_cast<char*>(pointer);
    pointer += sizeof(char);
    s.mcp_fromd = *reinterpret_cast<char*>(pointer);
    pointer += sizeof(char);
    s.no_hits = *reinterpret_cast<uint32_t*>(pointer);
    pointer += sizeof(uint32_t);
    s.hitIDs = {
        reinterpret_cast<uint32_t*>(pointer),
        reinterpret_cast<uint32_t*>(pointer + s.no_hits * sizeof(uint32_t))};
    pointer += s.no_hits * sizeof(uint32_t);
    p.particles.push_back(s);
  }
  p.parsed_size = std::distance(start, pointer);
  return p;
}

inline void print_mcparticle_segment(MCParticleSegment const& p) {
  std::cout << "\n*** Monte Carlo Particle Segment ***\n" << std::endl;
  std::cout << "no_mcp: " << p.no_mcp << std::endl;
  for (auto const& s : p.particles) {
    std::cout << "particle:" << std::endl;
    std::cout << "  mcp_key: " << s.mcp_key << std::endl;
    std::cout << "  mcp_id: " << s.mcp_id << std::endl;
    std::cout << "  mcp_p: " << s.mcp_p << std::endl;
    std::cout << "  mcp_pt: " << s.mcp_pt << std::endl;
    std::cout << "  mcp_eta: " << s.mcp_eta << std::endl;
    std::cout << "  mcp_phi: " << s.mcp_phi << std::endl;
    std::cout << "  flags: " << (s.mcp_islong != 0) << " "
              << (s.mcp_isdown != 0) << " " << (s.mcp_isvelo != 0) << " "
              << (s.mcp_isut != 0) << " " << (s.mcp_strangelong != 0) << " "
              << (s.mcp_strangedown != 0) << " " << (s.mcp_fromb != 0) << " "
              << (s.mcp_fromd != 0) << std::endl;
    std::cout << "  no_hits: " << s.no_hits << std::endl;
    std::cout << "  hitIDs:" << std::endl;
    for (auto const& h : s.hitIDs) {
      std::cout << "    " << h << std::endl;
    }
  }
}

/*** Track Segment ***/

inline TrackSegment get_track_segment(char* pointer) {
  char* start = pointer;
  TrackSegment t;
  t.no_tracks = *reinterpret_cast<uint32_t*>(pointer);
  pointer += sizeof(uint32_t);
  for (uint32_t i = 0; i < t.no_tracks; ++i) {
    TrackStruct s;
    s.no_hits = *reinterpret_cast<uint32_t*>(pointer);
    pointer += sizeof(uint32_t);
    s.hitIDs = {
        reinterpret_cast<uint32_t*>(pointer),
        reinterpret_cast<uint32_t*>(pointer + s.no_hits * sizeof(uint32_t))};
    pointer += s.no_hits * sizeof(uint32_t);
    t.tracks.push_back(s);
  }
  t.parsed_size = std::distance(start, pointer);
  return t;
}

inline void print_track_segment(TrackSegment const& t) {
  std::cout << "\n*** Track Segment ***\n" << std::endl;
  std::cout << "no_tracks: " << t.no_tracks << std::endl;
  for (auto const& s : t.tracks) {
    std::cout << "track:" << std::endl;
    std::cout << "  no_hits: " << s.no_hits << std::endl;
    std::cout << "  hitIDs:" << std::endl;
    for (auto const& h : s.hitIDs) {
      std::cout << "    " << h << std::endl;
    }
  }
}

}  // namespace dataformat

#endif
